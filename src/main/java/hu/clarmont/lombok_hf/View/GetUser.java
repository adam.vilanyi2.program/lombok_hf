package hu.clarmont.lombok_hf.View;

import hu.clarmont.lombok_hf.DTO.Car;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class GetUser {
    long id;
    String name;
    LocalDate birthDate;
    List<Car> cars;
}
