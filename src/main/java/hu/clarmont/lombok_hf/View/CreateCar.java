package hu.clarmont.lombok_hf.View;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateCar {
    String brand;
    String type;
}
