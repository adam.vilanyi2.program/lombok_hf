package hu.clarmont.lombok_hf.Controllers;

import hu.clarmont.lombok_hf.DTO.User;
import hu.clarmont.lombok_hf.View.CreateUser;
import hu.clarmont.lombok_hf.View.GetUser;
import hu.clarmont.lombok_hf.View.UpdateUser;
import jakarta.websocket.server.PathParam;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class UserController {
    long nextID = 0;
    Map<Long, User> users = new HashMap<>();

    @GetMapping("/user")
    public List<GetUser> getUser(@RequestParam(value = "id", required = false) Long id) {
        //no ID given
        if (id == null) {
            return new ArrayList<>(users.values().stream().map(user -> {
                GetUser getuser = new GetUser();
                getuser.setId(user.getId());
                getuser.setName(user.getName());
                getuser.setBirthDate(user.getBirthDate());
                return getuser;
            }).toList());
        }

        //return requested user
        List<GetUser> output = new ArrayList<>();
        User user = users.get(id);
        if (user == null) throw new IllegalArgumentException("No such user found");
        GetUser getuser = new GetUser();
        getuser.setId(user.getId());
        getuser.setName(user.getName());
        getuser.setBirthDate(user.getBirthDate());
        output.add(getuser);
        return output;
    }

    @PostMapping("/user")
    public void postUser(@RequestBody CreateUser createuser) {
        User user = new User();
        user.setId(nextID);
        user.setName(createuser.getName());
        user.setBirthDate(createuser.getBirthDate());
        user.setCars(createuser.getCars());
        users.put(nextID++, user);
    }

    @PutMapping("/user")
    public void putUser(@PathParam(value="id") Long id, @RequestBody UpdateUser updateuser) {
        User user = users.get(id);

        //check if user exist
        if (user == null) throw new IllegalArgumentException("No such user found");

        user.setCars(updateuser.getCars());

        users.put(id, user);
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable(value="id") long id) {
        //check if user exist
        if (users.get(id) == null) throw new IllegalArgumentException("No such user found");

        users.remove(id);
    }
}
