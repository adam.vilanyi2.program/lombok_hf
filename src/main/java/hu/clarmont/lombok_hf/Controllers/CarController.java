package hu.clarmont.lombok_hf.Controllers;

import hu.clarmont.lombok_hf.DTO.Car;
import hu.clarmont.lombok_hf.View.CreateCar;
import hu.clarmont.lombok_hf.View.GetCar;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class CarController {
    long nextID = 0;
    Map<Long, Car> cars = new HashMap<>();

    @GetMapping("/car")
    public List<GetCar> getCar(@RequestParam(value="id", required = false) Long id) {
        //no ID given
        if (id == null) {
            return new ArrayList<>(cars.values().stream().map(car -> {
                GetCar getcar = new GetCar();
                getcar.setId(car.getId());
                getcar.setBrand(car.getBrand());
                getcar.setType(car.getType());
                return getcar;
            }).toList());
        }

        //return requested car
        List<GetCar> output = new ArrayList<>();
        Car car = cars.get(id);
        if (car == null) throw new IllegalArgumentException("No such car found");
        GetCar getcar = new GetCar();
        getcar.setId(car.getId());
        getcar.setBrand(car.getBrand());
        getcar.setType(car.getType());
        output.add(getcar);
        return output;
    }

    @PostMapping("/car")
    public void postCar(@RequestBody CreateCar createcar) {
        Car car = new Car();
        car.setId(nextID);
        car.setBrand(createcar.getBrand());
        car.setType(createcar.getType());
        cars.put(nextID++, car);
    }

    @DeleteMapping("/car/{id}")
    public void deleteUser(@PathVariable(value="id") long id) {
        //check if car exist
        if (cars.get(id) == null) throw new IllegalArgumentException("No such car found");

        cars.remove(id);
    }
}
