package hu.clarmont.lombok_hf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LombokHfApplication {

	public static void main(String[] args) {
		SpringApplication.run(LombokHfApplication.class, args);
	}

}
