package hu.clarmont.lombok_hf.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Car {
    long id;
    String brand;
    String type;
}
