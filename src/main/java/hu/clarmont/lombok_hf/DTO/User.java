package hu.clarmont.lombok_hf.DTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class User {
    long id;
    String name;
    LocalDate birthDate;
    List<Car> cars;
}
